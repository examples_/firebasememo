package com.nalive.demomemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

public class MemoActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FirebaseAuth mAuth;
    FirebaseUser mUser;
    FirebaseDatabase db;
    EditText editMemo;
    private TextView text_name;
    private TextView text_email;
    private NavigationView navigationView;
    private String selectedKey;

    static {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memo);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editMemo = (EditText) findViewById(R.id.edit_memo);

        findViewById(R.id.fab_new_memo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

                clearMemo();
            }
        });

        findViewById(R.id.fab_save_memo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                if (TextUtils.isEmpty(selectedKey)) {
                    saveMemo();
                } else {
                    updateMemo();
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().clear();

        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        db = FirebaseDatabase.getInstance();


        text_name = (TextView) navigationView.getHeaderView(0).findViewById(R.id.text_name);
        text_email = (TextView) navigationView.getHeaderView(0).findViewById(R.id.text_email);

        updateProfile();

        initMemoList();
    }

    private void initMemoList() {
        db.getReference("memos/" + getUid() + "/").addChildEventListener(
                new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Memo memo = dataSnapshot.getValue(Memo.class);
                        memo.key = dataSnapshot.getKey();
                        displayMemoList(memo);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        Memo memo = dataSnapshot.getValue(Memo.class);
                        memo.key = dataSnapshot.getKey();

                        for (int i=0; i<navigationView.getMenu().size(); i++) {
                            MenuItem item = navigationView.getMenu().getItem(i);
                            Memo targetMemo = (Memo)item.getActionView().getTag();
                            if (memo.key.equals(targetMemo.key)) {
                                item.getActionView().setTag(memo);
                                item.setTitle(memo.getTitle());
                                break;
                            }

                        }
                        displayMemoList(memo);
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        String key = dataSnapshot.getKey();

                        for (int i=0; i<navigationView.getMenu().size(); i++) {
                            MenuItem item = navigationView.getMenu().getItem(i);
                            Memo targetMemo = (Memo)item.getActionView().getTag();
                            if (key.equals(targetMemo.key)) {
//                                int id = item.getItemId();
//                                navigationView.getMenu().removeItem(id); 바로 윗쪽 아이템을 지우네.
                                item.setVisible(false);
                                break;
                            }

                        }

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
    }

    private void displayMemoList(Memo memo) {
        Menu menu = navigationView.getMenu();

        MenuItem item = menu.add(memo.getTitle());
        View view = new View(getBaseContext());
        view.setTag(memo);
        item.setActionView(view);
    }

    private void updateProfile() {
        text_name.setText(mUser.getDisplayName());
        text_email.setText(mUser.getEmail());
    }

    private void saveMemo() {
        if (editMemo.getText().toString().isEmpty()) {
            return;
        }

        Memo memo = new Memo();
        memo.txt = editMemo.getText().toString();
        memo.createDate = System.currentTimeMillis();
        memo.owner = mUser.getUid();

        String uri = "memos/" + getUid() + "/";
        db.getReference(uri)
                .push()
                .setValue(memo)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(editMemo, "메모가 저장되었습니다", Snackbar.LENGTH_LONG).show();
                    }
                });

        clearMemo();
    }

    private void updateMemo() {
        Memo memo = new Memo();
        memo.txt = editMemo.getText().toString();
        memo.updateDate = System.currentTimeMillis();
        memo.owner = mUser.getUid();

        String uri = "memos/" + getUid() + "/" + selectedKey;
        db.getReference(uri)
                .setValue(memo)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(editMemo, "메모가 수정되었습니다", Snackbar.LENGTH_LONG).show();
                    }
                });

        clearMemo();
    }

    private void removeMemo() {
        if (selectedKey == null) {
            return;
        }

        Snackbar.make(editMemo, "메모를 삭제하시겠습니꽈아?", Snackbar.LENGTH_LONG)
                .setAction("삭제", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String uri = "memos/" + getUid() + "/" + selectedKey;
                        db.getReference(uri)
                                .removeValue()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Snackbar.make(editMemo, "삭제가 완료되었습니다", Snackbar.LENGTH_LONG).show();
                                    }
                                });

                        clearMemo();

                    }
                })
                .show();
    }

    private String getUid() {
        return "uid"; // 그냥 공통으로 메모 사용하기 위함
    }

    private void clearMemo() {
        editMemo.getText().clear();
        selectedKey = null;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.memo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_remove) {
            removeMemo();
            return true;
        } else if (id == R.id.action_sign_out) {
            signOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void signOut() {
        Snackbar.make(editMemo, "Sign out?", Snackbar.LENGTH_LONG)
                .setAction("Sign OUT!", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAuth.signOut();
                        finish();
                        startActivity(new Intent(MemoActivity.this, AuthActivity.class));
                    }
                })
                .show();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);

        Memo selectedMemo = (Memo)item.getActionView().getTag();
        editMemo.setText(selectedMemo.txt);
        selectedKey = selectedMemo.key;
        
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
