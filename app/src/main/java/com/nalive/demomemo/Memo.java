package com.nalive.demomemo;

/**
 * Created by holykss on 2017. 5. 14..
 */

public class Memo {
    public String key;
    public String txt;
    public long updateDate;
    public long createDate;

    public String owner;

    public String getTitle() {
        if (txt.contains("\n")) {
            return txt.substring(0, txt.indexOf("\n"));
        }
        return txt.substring(0, Math.min(txt.length(), 20));
    }
}
